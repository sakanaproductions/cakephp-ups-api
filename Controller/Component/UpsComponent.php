<?php
/**
 * UpsComponent
 *
 * A component that handles the UPS API
 *
 * PHP version 5
 *
 * @package		UpsComponent
 * @author		Jamie Fishback <jamie@sakanaproductions.net>
 * @link		https://github.com/sakanaproductions/cakephp-ups-api
 */

App::uses('Component', 'Controller');

/**
 * UpsComponent
 *
 * @package		UpsComponent
 */
class UpsComponent extends Component {

/**
 * Default Ups mode to use: Test or Production
 *
 * @var string
 * @access public
 */
	public $mode = 'Test';

/**
 * The required Ups Access Key
 *
 * @var string
 * @access public
 */
	public $key = null;

/**
 * Controller startup. Loads the Ups API library and sets options from
 * APP/Config/bootstrap.php.
 *
 * @param Controller $controller Instantiating controller
 * @return void
 * @throws CakeException
 * @throws CakeException
 */
	public function startup(Controller $controller) {
		$this->Controller = $controller;
        
		// Load the UPS class IF it hasn't been autolaoded (composer)
		App::import('Vendor', 'Ups.Ups', array(
			'file' => 'Ups' . DS . 'Ups.php')
		);
	
		if (!class_exists('Ups')) {
			throw new CakeException('Stripe API Libaray is missing or could not be loaded.');
		}

		// if mode is set in bootstrap.php, use it. otherwise, Test.
		$mode = Configure::read('Ups.mode');
		if ($mode) {
    		$this->mode = $mode;
		}

		// set the Stripe API key
		$this->key = Configure::read('Ups.' . $this->mode . 'Key');
		if (!$this->key) {
			throw new CakeException('Stripe API key is not set.');
		}
	}
}